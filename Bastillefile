# configure packages to be latest
CMD mkdir -p /usr/local/etc/pkg/repos
CMD echo 'FreeBSD: { url: "pkg+http://pkg.FreeBSD.org/${ABI}/latest" }' > /usr/local/etc/pkg/repos/FreeBSD.conf

# add lines to fstab for xorg to work correctly
CMD echo 'proc	/proc	procfs	rw	0	0' >> /etc/fstab
CMD echo 'fdesc	/dev/fd	fdescfs	rw,auto,late	0	0' >> /etc/fstab

# set language parameters
CMD echo 'LANG=en_US.UTF-8;   export LANG' >> /etc/profile
CMD echo 'CHARSET=UTF-8;  export CHARSET' >> /etc/profile

# memory settings for running a desktop
CMD echo 'kern.ipc.shmmax=67108864' >> /etc/sysctl.conf
CMD echo 'kern.ipc.shmall=32768' >> /etc/sysctl.conf
CMD echo 'kern.sched.preempt_thresh=224' >> /etc/sysctl.conf
CMD echo 'kern.maxfiles=200000' >> /etc/sysctl.conf
CMD echo 'kern.ipc.shm_allow_removed=1' >> /etc/sysctl.conf
CMD echo 'vfs.usermount=1' >> /etc/sysctl.conf

# load drivers in loader.conf
CMD echo 'autoboot_delay=2' >> /boot/loader.conf
CMD echo 'boot_mute="YES"' >> /boot/loader.conf
CMD echo 'kern.ipc.shmseg=1024' >> /boot/loader.conf
CMD echo 'kern.ipc.shmmni=1024' >> /boot/loader.conf
CMD echo 'kern.maxproc=100000' >> /boot/loader.conf
CMD echo 'mmc_load="YES"' >> /boot/loader.conf
CMD echo 'mmcsd_load="YES"' >> /boot/loader.conf
CMD echo 'sdhci_load="YES"' >> /boot/loader.conf
CMD echo 'atapicam_load="YES"' >> /boot/loader.conf
CMD echo 'fuse_load="YES"' >> /boot/loader.conf
CMD echo 'tmpfs_load="YES"' >> /boot/loader.conf
CMD echo 'aio_load="YES"' >> /boot/loader.conf
CMD echo 'libiconv_load="YES"' >> /boot/loader.conf
CMD echo 'libmchain_load="YES"' >> /boot/loader.conf
CMD echo 'cd9660_iconv_load="YES"' >> /boot/loader.conf
CMD echo 'msdosfs_iconv_load="YES"' >> /boot/loader.conf
CMD echo 'cuse_load="YES"' >> /boot/loader.conf
# Next line load nvidia driver
CMD echo 'nvidia-modeset_load="YES"' >> /boot/loader.conf
CMD echo 'snd_driver_load="YES"' >> /boot/loader.conf

# rc.conf driver settings
SYSRC rc_startmsgs="NO" 
SYSRC hcsecd_enable="YES"
SYSRC sdpd_enable="YES"
SYSRC ntpd_flags="-g"
SYSRC hald_enable="YES"
SYSRC dbus_enable="YES"
SYSRC linux_enable="YES"
SYSRC linux64_enable="YES"
SYSRC webcamd_enable="YES"
SYSRC lightdm_enable="YES"

# configure hardware settings
CMD echo '# Allow all users to access optical media' >> /etc/devfs.conf
CMD echo 'perm    /dev/acd0       0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/acd1       0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/cd0        0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/cd1        0666' >> /etc/devfs.conf
CMD echo ' ' >> /etc/devfs.conf
CMD echo '# Allow all USB Devices to be mounted' >> /etc/devfs.conf
CMD echo 'perm    /dev/da0        0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/da1        0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/da2        0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/da3        0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/da4        0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/da5        0666' >> /etc/devfs.conf
CMD echo ' ' >> /etc/devfs.conf 
CMD echo '# Misc other devices' >> /etc/devfs.conf
CMD echo 'perm    /dev/pass0      0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/xpt0       0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/uscanner0  0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/video0     0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/tuner0     0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/dvb/adapter0/demux0    0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/dvb/adapter0/dvr       0666' >> /etc/devfs.conf
CMD echo 'perm    /dev/dvb/adapter0/frontend0 0666' >> /etc/devfs.conf

# configure hardware devfs.rules 
CMD echo '[devfsrules_common=7]' >> /etc/devfs.rules
CMD echo 'add path 'ad[0-9]\*'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'ada[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'da[0-9]\*'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'acd[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'cd[0-9]\*'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'mmcsd[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'pass[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'xpt[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'ugen[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'usbctl'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'usb/\*'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'lpt[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'ulpt[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'unlpt[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'fd[0-9]\*'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'uscan[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'video[0-9]\*'	mode 666' >> /etc/devfs.rules
CMD echo 'add path 'tuner[0-9]*'  mode 666' >> /etc/devfs.rules
CMD echo 'add path 'dvb/\*'		mode 666' >> /etc/devfs.rules
CMD echo 'add path 'cx88*' mode 0660' >> /etc/devfs.rules
CMD echo 'add path 'cx23885*' mode 0660 # CX23885-family stream configuration device' >> /etc/devfs.rules
CMD echo 'add path 'iicdev*' mode 0660' >> /etc/devfs.rules
CMD echo 'add path 'uvisor[0-9]*' mode 0660' >> /etc/devfs.rules

# enable devfs rules
CMD echo 'devfs_system_ruleset="devfsrules_common"' >> /etc/rc.conf

# install xorg and utilities
PKG xorg vim feh terminator wget curl firefox

# install nvidia video files
PKG nvidia-driver nvidia-settings nvidia-xconfig

# install lightdm display manager 
PKG lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings

# i3 window manager
PKG i3 i3status i3blocks dmenu rofi v4l-utils v4l_compat webcamd

# write initial xorg.conf file for nvidia driver
CMD nvidia-xconfig

# reboot to start system with all changes
CMD reboot
