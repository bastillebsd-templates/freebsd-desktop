# freebsd-desktop



## Getting started

This rocinante template is to set up a FreeBSD desktop with Nvidia driver, lightdm, and i3 wm.

Once freebsd is installed do the following steps to run this template
        # pkg install rocinante
        
        # rocinante bootstrap freebsd-desktop

        # rociante template freebsd-desktop

## list of main packages included in this template

        # Xorg
        # i3 window manager
        # Nvidia video drivers
        # lightdm display manager
        # vim
        # terminator



IF you do not want Nvidia drivers then comment out line 40 to not install the driver in the loader.conf.  Also remove/comment out line 111 to not install nvidia drivers.

If you want a different window manager other that i3, then change the packages listed
on line 115 to remove the i3 packages and add the packages  for your favorite window manager, xfce, mate, etc.

If you want to add more packages to be installed either add the packages to line 109, or add another PKG line to the end
of the list of PKG statements.
        
Once this is complete before you reboot run the following commands as your user to set up your i3 with my config file:

        # mkdir ~/.config/i3
        # cp config ~/.config/i3/config

This will make it run the config at login of i3.  If you want the default and to configure it yourself, do not copy it.

If you want a cheat sheet for the keystrokes with this config go to the following link to download it:

https://snarfingcode666.com/posts/i3-wm/i3_cheatsheet 



        
